"""
Rich File Manager for Flask
"""

from .filemanager import filemanager_blueprint as blueprint
from .filemanager import FileManager

__author__ = 'Stephen Brown (Little Fish Solutions LTD)'

